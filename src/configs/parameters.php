<?php
// If sf 3.3 and no annotations is installed, then disable it.
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;

/** @var ContainerBuilder $container */
if (Kernel::VERSION_ID >= 30300 && !class_exists('Doctrine\Common\Annotations\Annotation')) {
    $container->loadFromExtension('framework', [
        'annotations' => ['enabled' => false],
    ]);
} else {
    $container->loadFromExtension('framework', [
        'annotations' => ['cache' => 'none'],
    ]);
}

// Not setting the router to utf8 is deprecated in symfony 5.1
if (Kernel::VERSION_ID >= 50100) {
    $container->loadFromExtension('framework', [
        'router' => ['utf8' => true],
    ]);
}
