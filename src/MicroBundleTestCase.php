<?php

namespace Bricre\SymfonyTest;

use LogicException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

abstract class MicroBundleTestCase extends TestCase
{
    protected string $kernelClass = MicroKernel::class;

    /**
     * @var KernelInterface|null
     */
    private ?KernelInterface $kernel = null;

    /**
     * @var CompilerPassInterface[]
     */
    private array $compilerPasses = [];

    public function setKernelClass(string $kernelClass): void
    {
        $this->kernelClass = $kernelClass;
    }

    protected function addCompilerPass(CompilerPassInterface $compilerPass, string $index = null): void
    {
        $this->compilerPasses[$index ?: get_class($compilerPass)] = $compilerPass;
    }

    /**
     * Boots the Kernel for this test.
     */
    protected function bootKernel(): void
    {
        $this->ensureKernelShutdown();

        if (null === $this->kernel) {
            $this->createKernel();
        }

        $this->kernel->boot();
    }

    /**
     * Shuts the kernel down if it was used in the test.
     */
    public function ensureKernelShutdown(): void
    {
        if (null !== $this->kernel) {
            try {
                $this->kernel->getContainer();
            } catch (LogicException $e) {
                $container = null;
            }
            $this->kernel->shutdown();
        }
    }

    /**
     * Get a kernel which you may configure with your bundle and services.
     */
    protected function createKernel(): KernelInterface
    {
        if (!class_exists(Kernel::class)) {
            throw new LogicException('You must at least install symfony/http-kernel to run the test.');
        }

        $this->kernel = new $this->kernelClass($_SERVER['APP_ENV'] ?? 'prod');
        $this->kernel->addBundle($this->getBundleClass());
        $this->kernel->addCompilerPasses($this->compilerPasses);

        return $this->kernel;
    }

    abstract protected function getBundleClass(): string;

    protected function getContainer(): ContainerInterface
    {
        return $this->kernel->getContainer();
    }
}
