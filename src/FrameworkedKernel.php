<?php


namespace Bricre\SymfonyTest;


use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

class FrameworkedKernel extends MicroKernel
{
    use MicroKernelTrait {
        MicroKernelTrait::registerContainerConfiguration as registerContainerConfigurationTrait;
    }

    public function __construct(string $cachePrefix)
    {
        parent::__construct($cachePrefix);
        $this->addBundle(FrameworkBundle::class);
        $this->addConfigFile(__DIR__ . '/configs/framework.yml');
    }


    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        parent::registerContainerConfiguration($loader);
        $this->registerContainerConfigurationTrait($loader);
    }

    protected function configureContainer(ContainerConfigurator $c): void
    {

    }

    public function registerBundles(): iterable
    {
        return parent::registerBundles();
    }


}