<?php


namespace Bricre\SymfonyTest;


abstract class FrameworkedBundleTestCase extends MicroBundleTestCase
{

	protected function setUp(): void
	{
		parent::setUp();
		$this->setKernelClass(FrameworkedKernel::class);
	}
}