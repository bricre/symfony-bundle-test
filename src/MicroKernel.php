<?php

namespace Bricre\SymfonyTest;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\Config\Exception\LoaderLoadException;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouteCollectionBuilder;

class MicroKernel extends Kernel
{
    use MicroKernelTrait;

    protected array $bundlesToRegister = [];
    protected array $configFiles = [];
    protected string $cachePrefix;
    protected ?string $projectDir = null;
    protected ?string $fakeProjectDir = null;

    /**
     * @var CompilerPassInterface[]
     */
    protected array $compilerPasses = [];


    /**
     * @param  string  $cachePrefix
     */
    public function __construct(string $cachePrefix)
    {
        parent::__construct($cachePrefix, true);
        $this->cachePrefix = $cachePrefix;
    }

    /**
     * @param  string  $bundleClassName
     */
    public function addBundle(string $bundleClassName)
    {
        $this->bundlesToRegister[] = $bundleClassName;
    }

    /**
     * @param  string  $configFile  path to configs file
     */
    public function addConfigFile(string $configFile)
    {
        $this->configFiles[] = $configFile;
    }

    public function getCacheDir(): string
    {
        return sys_get_temp_dir() . '/symfony-bundle-test/' . $this->cachePrefix;
    }

    public function getLogDir(): string
    {
        return sprintf("%s/symfony-bundle-test/log", sys_get_temp_dir());
    }

    public function registerBundles(): iterable
    {
        $this->bundlesToRegister = array_unique($this->bundlesToRegister);
        $bundles                 = [];
        foreach ($this->bundlesToRegister as $bundle) {
            $bundles[] = new $bundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $container) use ($loader) {
            $this->configFiles = array_unique($this->configFiles);
            foreach ($this->configFiles as $path) {
                $loader->load($path);
            }

            $container->addObjectResource($this);
        });
    }

    /**
     * @param  LoaderInterface  $loader
     *
     * @return RouteCollection
     * @throws LoaderLoadException
     */
    public function loadRoutes(LoaderInterface $loader)
    {
        $routes = new RouteCollectionBuilder($loader);

        $routes->import($this->getProjectDir() . '/configs/*.{yaml,php}');

        return $routes->build();
    }

    public function getProjectDir()
    {
        if (null !== $this->fakeProjectDir) {
            $this->projectDir = $this->fakeProjectDir;
        }

        if (null === $this->projectDir) {
            if (realpath(__DIR__ . '/../../../../configs')) {
                $this->projectDir = realpath(__DIR__ . '/../../../../');
            } else {
                $this->projectDir = realpath(__DIR__ . '/../');

            }
        }

        return $this->projectDir;
    }

    /**
     * @param  CompilerPassInterface[]  $compilerPasses
     */
    public function addCompilerPasses(array $compilerPasses): void
    {
        $this->compilerPasses = $compilerPasses;
    }

    /**
     * {@inheritdoc}
     */
    protected function buildContainer(): ContainerBuilder
    {
        $container = parent::buildContainer();

        foreach ($this->compilerPasses as $pass) {
            $container->addCompilerPass($pass);
        }

        return $container;
    }

    /**
     * @param  string|null  $fakeProjectDir
     */
    public function setFakeProjectDir(?string $fakeProjectDir): void
    {
        $this->fakeProjectDir = $fakeProjectDir;
    }
}
