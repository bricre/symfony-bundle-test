<?php

namespace Bricre\SymfonyTest;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

final class PublicForTestsCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container): void
	{
		if (!$this->isPHPUnit()) {
			return;
		}

		foreach ($container->getDefinitions() as $definition) {
			$definition->setPublic(TRUE);
		}

		foreach ($container->getAliases() as $definition) {
			$definition->setPublic(TRUE);
		}
	}

	private function isPHPUnit(): bool
	{
		// there constants are defined by PHPUnit
		return defined('PHPUNIT_COMPOSER_INSTALL') || defined('__PHPUNIT_PHAR__');
	}
}