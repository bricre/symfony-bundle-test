<?php


namespace Bricre\SymfonyTest;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
class PublicServicePass implements CompilerPassInterface
{
	/**
	 * A regex to match the services that should be public.
	 */
	private string $regex;

	public function __construct(string $regex = '|.*|')
	{
		$this->regex = $regex;
	}

	/**
	 * @param ContainerBuilder $container
	 */
	public function process(ContainerBuilder $container)
	{
		foreach ($container->getDefinitions() as $id => $definition) {
			if (preg_match($this->regex, $id)) {
				$definition->setPublic(TRUE);
			}
		}

		foreach ($container->getAliases() as $id => $alias) {
			if (preg_match($this->regex, $id)) {
				$alias->setPublic(TRUE);
			}
		}
	}
}